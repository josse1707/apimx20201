#imagen base
FROM node:latest

#directorio de la aplicacion
WORKDIR /app

#copiado de archivos
ADD . /app

#dependencias
RUN npm install

#puerto que expongo
EXPOSE 3000

#comando
CMD ["npm", "start"]
