var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');



var requestjson=require('request-json');

var urlClientes='https://api.mlab.com/api/1/databases/jarango/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt&s={%22apellido%22:-1}'


var bodyParser=require('body-parser');
app.use(bodyParser.json())
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var movimientosJSON=require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req,res){
  res.sendFile(path.join(__dirname,'index.html'));
});

app.get('/Clientes/:idcliente', function (req,res){
  res.send('Aqui tiene al cliente número: ' + req.params.idcliente);
});


app.post('/', function (req,res){
  res.send('Hemos recibido su peticion post');
});

app.put('/', function (req,res){
  res.send('Hemos recibido su peticion put cambiada');
});

app.delete('/', function (req,res){
  res.send('Hemos recibido su peticion delete');
});


app.get('/v1/movimientos',function(req,res){
  res.sendfile('movimientosv1.json')
})

app.get('/v2/movimientos',function(req,res){
  res.json(movimientosJSON)
})

app.get('/v2/movimientos/:indice',function(req,res){
  console.log(req.params.indice);
  res.send(movimientosJSON[req.params.indice])
})

app.get('/v3/movimientosquery',function(req,res){
  console.log(req.query);
  res.send('recibido')
})

app.post('/v3/movimientos', function(req,res){
  var nuevo=req.body
  nuevo.id=movimientosJSON.length + 1
  movimientosJSON.push(nuevo)
  res.send('movimiento dado de alta')
})


app.get('/Clientes',function(req,res){
  var clientesMLab=requestjson.createClient(urlClientes)
  clientesMLab.get('',function(err,resM,body){
    if(err){
      console.log(body);
    }
    else{
      res.send(body);
    }
  })
})
